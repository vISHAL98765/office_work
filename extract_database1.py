import sqlite3
import pandas as pd

db = sqlite3.connect('co2_sample2.db')


def tables_in_sqlite_db(conn):
    cursor = conn.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = [
        v[0] for v in cursor.fetchall()
        if v[0] != "sqlite_sequence"
    ]
    cursor.close()
    return tables


# print(tables_in_sqlite_db(db))
writer = pd.ExcelWriter('work_new_1.xlsx',engine = 'xlsxwriter')

for i in tables_in_sqlite_db(db):
    data = pd.read_sql_query(f"SELECT *FROM {i};",db)
    if len(data)>0:
        data.to_excel(writer,sheet_name = i[0:31],index = False)

writer.save()
